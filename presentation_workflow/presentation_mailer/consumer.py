import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        def process_approval(ch,method,properties,body):
            message_dict = json.loads(body)
            name = message_dict["presenter_name"]
            title = message_dict["title"]
            email = [message_dict["presenter_email"]]
            send_mail(
                "Your presentation has been accepted",
                f"{name}, we're happy to tell you that your presentation {title} has been accepted",
                "admin@conference.go",
                email,
                fail_silently=False,
            )


        def process_rejection(ch,method,properties,body):
            message_dict = json.loads(body)
            name = message_dict["presenter_name"]
            title = message_dict["title"]
            email= [message_dict["presenter_email"]]
            send_mail(
                "Your presentation has been rej ected",
                f"{name}, we're sorry to tell you that your presentation {title} has not been accepted",
                "admin@conference.go",
                recipient_list=email,
                fail_silently=False,
            )



        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
